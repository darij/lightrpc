#!/usr/bin/env perl
# t ����� [-3..1] [���� �����1 ...] - 1-� �������� - verbosity

use TAP::Harness;
use Term::ANSIColor qw(:constants);



chdir "t";
if($ARGV[0] =~ /^-?\d+$/) {
	$from = 1;
	$verbosity = $ARGV[0];
} else {
	$from = 0;
	$verbosity = 0; # -3;
}

my @test = map { my $u = $_; my @t = grep { -e $_ } ($_, map { "$u.$_" } qw(t t.php t.py t.rb)); @t[0] } @ARGV[$from..$#ARGV];
@test = (<*.t>, <*.t.*>) unless @test;
my $harness = TAP::Harness->new({
	verbosity => $verbosity,		# -3, 1
	color => 1,
	timer => 1,
	lib => $lib = [".", "../../my", "../lib", ".."],
	exec => sub {
		my ( $harness, $test_file ) = @_;
		return [ qw( /usr/bin/env php ), $test_file ] if $test_file =~ /[.]php$/;
		return [ qw( /usr/bin/env python ), $test_file ] if $test_file =~ /[.]py$/;
		return [ (qw( /usr/bin/env ruby -w ), map {("-I", $_)} @$lib), $test_file ] if $test_file =~ /[.]rb$/;
		return undef;
	}
});
$harness->runtests(@test);
chdir "..";

