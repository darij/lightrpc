package rpc;

use Data::Walk;
use POSIX qw//;

our %prog = (
"perl" => "perl -I'%s' -e 'require rpc; rpc->minor'",
"php" => "php -r 'require_once \"%s/rpc.php\"; new rpc();'",
"python" => "python -c 'import sys; sys.path.append(\"%s\"); from rpc import RPC; RPC()'",
"ruby" => "ruby -I'%s' -e 'require \"rpc.rb\"; RPC.new'"
);

sub new {
	my ($cls, $prog) = @_;

	pipe $ch_reader, $writer or die "not create pipe. $!";
	pipe $reader, $ch_writer or die "not create pipe. $!";
	
	binmode $reader; binmode $writer; binmode $ch_reader; binmode $ch_writer;

	my $stdout = select $in; $| = 1;
	select $writer; $| = 1;
	select $ch_writer; $| = 1;
	select $stdout;
	
	my $pid = fork;
	
	die "fork. $!" if $pid < 0;
	
	unless($pid) {
		my $p = $prog{$prog};
		$p = sprintf $p, $INC{'rpc.pm'} =~ /\/rpc.pm$/ && $` if defined $p;
		my $ch4 = fileno $ch_reader;
		my $ch5 = fileno $ch_writer;
		POSIX::dup2($ch4, 4) if $ch4 != 4;
		POSIX::dup2($ch5, 5) if $ch5 != 5;
		exec $prog or die "������ �������� ������������. $!";
	}
		
	bless {r => $reader, w => $writer, prog => $prog, p=>$p, objects => {}, nums => [], warn=>0, role => "MAJOR"}, $cls;

}

# ������ �������
sub minor {
	my ($cls) = @_;

	open my $r, "<&=4" or die "NOT ASSIGN IN: $!";
	open my $w, ">&=5" or die "NOT ASSIGN OUT: $!";
	
	binmode $r; binmode $w;
	my $stdout = select $w; $| = 1;
	select $stdout;

	
	my $self = bless {r => $r, w => $w, objects => {}, nums => [], warn=>0, role => "MINOR"}, $cls;
	my @ret = $self->ret;
	warn "MINOR ENDED @ret" if $self->{warn};
	return @ret;
}



# ��������� ����������
sub close {
	my ($self) = @_;
	local ($,, $\) = ();
	$self->ok;
	close $self->{w} or die "�� ������ ����� ������";
	close $self->{r} or die "�� ������ ����� ������";
}

